module Main where

import Lib
import Carte 


import SDL
import SDL.Time (time, delay)
-- import Linear (V4(..))
import Control.Concurrent (threadDelay)

import qualified System.IO as I
import qualified Data.Map.Strict as M

import Data.List (foldl')

import Keyboard (Keyboard)
import qualified Keyboard as K

import Foreign.C.Types (CInt (..) )

import TextureMap (TextureMap, TextureId (..))
import qualified TextureMap as TM

import Sprite (Sprite)
import qualified Sprite as S

import SpriteMap (SpriteMap, SpriteId (..))
import qualified SpriteMap as SM

import Data.Set (Set)
import qualified Data.Set as Set

import Data.Text
import qualified Data.Text as Tex

import qualified Debug.Trace as T


arrierPlan :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
arrierPlan rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "arriere") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "arriere") (S.mkArea 0 0 1000 700)
  let smap' = SM.addSprite (SpriteId "arriere") sprite smap
  return (tmap', smap')

mur :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
mur rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "mur") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "mur") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "mur") sprite smap
  return (tmap', smap')

depart :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
depart rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "depart") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "depart") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "depart") sprite smap
  return (tmap', smap')



arriver :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
arriver rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "arriver") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "arriver") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "arriver") sprite smap
  return (tmap', smap')

vide :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
vide rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "vide") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "vide") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "vide") sprite smap
  return (tmap', smap')

porteEO :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
porteEO rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "porteEO") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "porteEO") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "porteEO") sprite smap
  return (tmap', smap')

porteNS :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
porteNS rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "porteNS") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "porteNS") (S.mkArea 0 0 60 60)
  let smap' = SM.addSprite (SpriteId "porteNS") sprite smap
  return (tmap', smap')


main :: IO ()
main =do 
    initializeAll
    window <- createWindow (Tex.pack "PAF_Projet") $ defaultWindow { windowInitialSize = V2 1000 700 }
    renderer <- createRenderer window (-1) defaultRenderer
    -- chargement de l'image du fond
    (tmap, smap) <- arrierPlan renderer "src/assets/arriere.jpg" TM.createTextureMap SM.createSpriteMap
    (tmap', smap') <- mur renderer "src/assets/mur.jpeg" tmap smap
    (tmap'', smap'') <- depart renderer "src/assets/depart.jpeg" tmap' smap'
    (tm, sm) <- arriver renderer "src/assets/arrive.jpeg" tmap'' smap''
    (tm', sm') <- porteEO renderer "src/assets/porteNS.jpg" tm sm
    (tm'', sm'') <- porteNS renderer "src/assets/porteNS.jpg" tm' sm'
    handle <- I.openFile "src/niveau.txt" I.ReadMode
    contenu <- I.hGetContents handle
    putStrLn (show contenu)
    S.displaySprite renderer tm'' (SM.fetchSprite (SpriteId "arriere") sm'')
    
    let cartes@(Carte c l carte )= readCarte contenu
    
    M.foldrWithKey (\(C x y)  v  acc ->
      case v of 
        Mur ->do 
            acc  
            S.displaySprite renderer tm'' (S.moveTo (SM.fetchSprite (SpriteId "mur") sm'')
                                 ( fromIntegral (if y == 0 then y else (y*60)))
                                 (  fromIntegral (if x == 0 then x else (x*60))))  
        Sortie ->do
            acc  
            S.displaySprite renderer tm'' (S.moveTo (SM.fetchSprite (SpriteId "arriver") sm'')
                                 ( fromIntegral (if y == 0 then y else (y*60)))
                                 (  fromIntegral (if x == 0 then x else (x*60)))) 
        Entree ->do
            acc 
            S.displaySprite renderer tm'' (S.moveTo (SM.fetchSprite (SpriteId "depart") sm'')
                                 ( fromIntegral (if y == 0 then y else (y*60)))

                                 (  fromIntegral (if x == 0 then x else (x*60)))) 
        Porte NS Fermee ->do
              acc
              S.displaySprite renderer tm'' (S.moveTo (SM.fetchSprite (SpriteId "porteNS") sm'')
                                 ( fromIntegral (if y == 0 then y else (y*60)))
                                 (  fromIntegral (if x == 0 then x else (x*60)))) 
        Porte EO Fermee ->do
              acc
              S.displaySprite renderer tm'' (S.moveTo (SM.fetchSprite (SpriteId "porteNS") sm'')
                                 ( fromIntegral (if y == 0 then y else (y*60)))
                                 (  fromIntegral (if x == 0 then x else (x*60)))) 
        otherwise ->do
              acc
     ) (putStrLn"affichage de la carte") carte
    present renderer
    threadDelay $ 12000000
     