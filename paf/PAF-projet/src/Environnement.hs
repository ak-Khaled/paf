module Environnement where 

import qualified Data.Map.Strict as M
import qualified Data.List as L
import Carte 

data Entite = Vache {iden :: Int , pvie :: Int}
    | Joueur {iden :: Int , pvie :: Int }
   


    deriving (Eq)

-- data decrivant l'environement du jeu des entites    
data Envi = Envi { contenu_envi :: M.Map Coord [ Entite ]}

-- ********************** show  *************************

-- 1) Entite  
show_entite :: Entite -> String 
show_entite (Vache id pvie )  = "V"
show_entite (Joueur id pvie ) = "J" 

instance Show Entite where 
    show = show_entite


-- ********************** read **************************

read_entite :: String -> Int -> Entite 
read_entite "J" id  = Joueur id  100
read_entite "V" id  = Vache id 20

instance Read Entite where 
    readsPrec id s = [((read_entite s id ),s )]


read_environnement :: [String] -> Envi 
read_environnement string  = undefined




-- ****************** fonction liés a l'environnement ****************** 

franchissable_env :: Coord -> Envi -> Bool
franchissable_env coord environement@(Envi envi) = case M.lookup coord envi of 
                                                    Nothing -> True
                                                    Just listeEntites -> foldr (\ valeur accu -> case valeur of 
                                                                                                 (Vache _ _ )-> False 
                                                                                                 _ -> True   
                                                        ) False listeEntites  

recuperationID :: Entite -> Int 
recuperationID (Joueur id _ ) = id
recuperationID (Vache id _ )  = id   

trouve_id :: Int -> Envi -> Maybe (Coord, Entite)
trouve_id n environement@(Envi envi ) = M.foldrWithKey (\coord v acc -> (foldr (\entite accu -> if (recuperationID entite) == n then 
                                                                                    Just(coord,entite)
                                                                                else
                                                                                    acc ) acc  v )         
                                        ) Nothing envi 

rm_env_id :: Int -> Envi -> Envi
rm_env_id n environement@(Envi envi) = let coord_entite = trouve_id n environement in
    case coord_entite of  
        Nothing -> environement
        Just (coord,entite) -> case M.lookup coord envi of  
                                Just listeEntites -> (Envi (M.insert coord (L.delete entite listeEntites ) envi)) 
                                Nothing -> environement

bouge_id :: Int -> Coord -> Envi -> Envi
bouge_id n coord environement@(Envi envi) = let coord_entite = trouve_id n environement in 
    case coord_entite of 
        Nothing -> environement 
        Just (coord ,entite ) ->let environement_without_entite_i_n =  rm_env_id n environement in 
            case M.lookup coord envi of 
                Nothing -> Envi (M.insert coord [entite] envi)
                Just listes_entites -> Envi (M.insert coord (entite:listes_entites) envi)
