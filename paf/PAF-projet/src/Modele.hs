module Modele where 

import Carte
import Keyboard
import Environnement

data Modele = Cont {
    carte :: Carte , 
    envi :: Envi , 
    log :: String , 
    keyboard :: Keyboard  -- l’etat du clavier
}

bouge :: Modele -> Entite -> Coord -> Modele
bouge model entite coord = undefined



-- ******************* ordre *******************
data Ordre = N | S | E | O | U | R deriving Show