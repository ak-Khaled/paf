module Carte where

import System.IO

import Keyboard (Keyboard)
import qualified Keyboard as K

import Data.Text
import qualified Data.Text as T
import qualified Data.Map.Strict as M
import qualified Data.List as L

data PDirection = NS | EO deriving Eq -- direction d’une porte

data StatutP = Ouverte | Fermee deriving Eq -- statut d’une porte

data Case = Normal -- une case vide
  | Porte PDirection StatutP -- une porte ouverte ou fermee
  | Mur -- infranchissable (sauf pour les fantomes ...)
  | Entree -- debut du niveau
  | Sortie -- fin du niveau
  deriving Eq


data Coord = C {cx :: Int , cy :: Int} deriving (Eq,Ord,Show)

data Carte = Carte { cartel :: Int , -- largeur
  carteh :: Int , -- hauteur
  carte_contenu :: (M.Map Coord Case) -- cases de la carte
  }


-- ************** SHOW *************

-- 1) Case :
show_case :: Case -> String
show_case Normal = " "
show_case (Porte _ Ouverte) = " "
show_case (Porte EO Fermee) = "|"
show_case (Porte NS Fermee) = "-"
show_case Mur = "X"
show_case Entree = "E"
show_case Sortie = "S"

instance Show Case where 
  show = show_case

-- 2) Case :

-- retourner Char depuis une carte 
show_carte :: Carte -> String 
show_carte c@(Carte largeur hauteur map) = 
  M.foldrWithKey (\corddonnee@(C x y)  valeur accumulateur -> 
                                                              if y == (largeur-1) then 
                                                                (accumulateur ++ (show_case valeur) ++ ("\n"))  
                                                              else 
                                                                accumulateur ++ (show_case valeur )) "" map 

-- instanciation Show  pour carte 
instance Show Carte where 
  show = show_carte 

-- ************** READ *************
recuperat_case :: Char -> Case
recuperat_case '|' = (Porte EO Fermee) 
recuperat_case '-' = (Porte NS Fermee) 
recuperat_case 'X' = Mur 
recuperat_case 'E' = Entree 
recuperat_case 'S' = Sortie
recuperat_case  _  = Normal 

-- instaciation de Read pour case 
instance Read Case where
  readsPrec _ b = [((recuperat_case (L.head b)),b)] 
 

-- *********************************************
readCarte :: String -> Carte
readCarte contenu =
    let lignes = T.splitOn (pack "\n") (pack contenu) in
        let (mapCarte, maxCol, maxLigne)= L.foldl(\(m, c, l) ligne -> 
             T.foldl (\(map, col, lig) caz -> ( (M.insert (C col lig) (recuperat_case caz) map), (col+1), lig )  ) (m, 0, (l+1)) ligne 
             ) (M.empty, 0, (-1))  lignes 
        in
            let Just ((C maxC maxL),_) = M.lookupMax mapCarte in
                Carte (maxC+1) (maxL+1) mapCarte



instance Read Carte where
  readsPrec _ string  = [((readCarte string),string)]  -- [((read_carte (S.split string "\n" )),string)]

-- ************** Invaraiant *************


cest_un_mur :: Coord -> Carte -> Bool 
cest_un_mur coord carte@(Carte _ _ map_carte ) = case M.lookup coord map_carte of 
                                                  Nothing -> False 
                                                  Just (Mur) -> True 
                                                  _ -> False 

invariant_toutes_case_existante_dans_carrer :: Carte -> Bool 
invariant_toutes_case_existante_dans_carrer carte@(Carte l h map_carte ) = undefined

invariant_case_encadrer :: Carte -> Bool
invariant_case_encadrer  carte@(Carte l h map_carte ) = M.foldrWithKey (\coord@(C x y ) _ accu -> accu && (x >= 0 ) && ( x < h ) && (y >= 0) && ( y < l) )  True map_carte 

invanriant_une_entree_une_sortie :: Carte -> Bool 
invanriant_une_entree_une_sortie carte@(Carte l h map_carte ) = let nombre_entre_sortie =  M.foldrWithKey (\k v accu -> case v of 
                                                                                              Entree -> (accu+1)
                                                                                              Sortie -> (accu+1)
                                                                                              ) 0 map_carte in 
                                                                                                nombre_entre_sortie == 2

invariant_entourage_murs :: Carte -> Bool
invariant_entourage_murs carte@(Carte l h map_carte ) = undefined 


invariant_porte_encadrer :: Carte -> Bool 
invariant_porte_encadrer carte@(Carte l h map_carte ) = M.foldrWithKey (\k@(C x y) v accu -> case v of 
                                                                                      Porte EO _ -> accu && (cest_un_mur (C (x-1) y) carte) && (cest_un_mur (C (x+1) y) carte) 
                                                                                      Porte NS _ -> accu && (cest_un_mur (C x (y-1)) carte) && (cest_un_mur (C x (y+1)) carte)
                                                                                      _ -> accu  
  ) True map_carte


-- ***************************************